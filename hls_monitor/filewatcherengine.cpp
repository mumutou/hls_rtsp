/*
 *  Copyright (C) 2010  Giorgio Wicklein <g.wicklein@giowisys.com>
 *
 *  This file is part of DaemonFS.
 *
 *  DaemonFS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DaemonFS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DaemonFS.  If not, see <http://www.gnu.org/licenses/>.
 */

//-----------------------------------------------------------------------------
// Hearders
//-----------------------------------------------------------------------------

#include <QFileSystemWatcher>
#include <QDebug>
#include <QDirIterator>
#include <QDateTime>
#include <qsettings.h>
#include "filewatcherengine.h"

//-----------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------

FileWatcherEngine::FileWatcherEngine(QObject *parent) : QObject(parent)
{
	fileSystemWatcher = NULL;
	fileMap = NULL;
}

FileWatcherEngine::~FileWatcherEngine()
{
    delete fileMap;
}

void FileWatcherEngine::addPaths(QStringList &paths)
{
    QStringList files;

    for (int i = 0; i < paths.size(); i++)
        getAllFiles(paths.at(i), files);

    qDebug() << "[DaemonFS]: Indexed files: " << files.size();

//	qDebug() << files;
//    fileSystemWatcher->addPaths(files);
}

void FileWatcherEngine::clear()
{
//    QStringList paths = fileMap->keys();
//    fileSystemWatcher->removePaths(paths);
    fileMap->clear();
}

bool FileWatcherEngine::isDeleted(const QString &path, QString &target_file)
{
	bool somethingDelete = false;

	if (QFileInfo(path).isDir()) {
		QFileInfoList files = QDir(path).entryInfoList(QDir::Files | QDir::NoDotAndDotDot | QDir::Hidden);
		QStringList tmp;
		for (int j = 0; j < files.size(); j++) {
			tmp << files.at(j).absoluteFilePath();
		}
		//qDebug() << *fileMap;
		for (int i = 0; i < fileMap->size(); i++) {
			QString f = fileMap->at(i);
			if (tmp.contains(f)) {

			} else {
				somethingDelete = true;
				target_file = fileMap->at(i);
				break;
			}
		}
		if (somethingDelete)
			return true;
		else
			return false;
	} else {
		return false;
	}
}

bool FileWatcherEngine::isNew(const QString& path, QString& target_file)
{
    bool somethingNew = false;

    if (QFileInfo(path).isDir()) {
        QFileInfoList files = QDir(path).entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);
        for (int i = 0; i < files.size(); i++) {
            if (!fileMap->contains(files.at(i).absoluteFilePath())) {
                somethingNew = true;
				target_file = files.at(i).absoluteFilePath();
            }
        }
        if (somethingNew)
            return true;
        else
            return false;
    }
    else {
        return false;
    }
}

//-----------------------------------------------------------------------------
// Private slots
//-----------------------------------------------------------------------------

void FileWatcherEngine::fileChanged(QString path)
{
    //qDebug() << QString("This file has been changed: ") << path;

//    emit fschanged(path);
	//QString action;
	//QString lastModified;

//	Q_ASSERT(mainWindow != 0);
	//qDebug() << path;
	QString target = QString("");
	if (isNew(path, target)) {
		//action = tr("content modified");
		qDebug() << target << " :is new";
		QFileInfo fi(target);

		if (fi.isFile()) {
			ffmpeg_start(target);
			fileMap->push_back(target);
		}
	} else if (isDeleted(path, target)) {
		//action = tr("deleted");
		qDebug() << target << " :is deleted";
		ffmpeg_close(target);
		fileMap->removeOne(target);
	} else {
		//action = tr("modified");
		qDebug() << path << " :is modified";
	}
}

void FileWatcherEngine::dirChanged(QString path)
{
    qDebug() << QString("This directory has been changed: ") << path;

	fileChanged(path);

}

//-----------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------

void FileWatcherEngine::getAllFiles(QString path, QStringList &result)
{
    QFileInfo file(path);
	
    if (file.isDir()) {

        // Add top directroy
        fileSystemWatcher->addPath(file.absoluteFilePath());
		//fileMap->push_back(file.absoluteFilePath());// , QFileInfo(file.absoluteFilePath()).lastModified());

        // Add all subfiles and subdirs
		QDirIterator it(path, QDir::Files | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories);

        while (it.hasNext()) {
            QString tmp = it.next();
            result.append(tmp);
			ffmpeg_start(tmp);
			fileMap->push_back(tmp);// , QFileInfo(tmp).lastModified());
        }

    } else {
	
		result.append(file.absoluteFilePath());
		ffmpeg_start(file.absoluteFilePath());
		fileMap->push_back(file.absoluteFilePath());// , file.lastModified());

    }
}

#include "ServiceManager.h"
#include "BaseService.h"
#include <qcoreapplication.h>
void FileWatcherEngine::ffmpeg_start(const QString& file) {
	if (processMap.contains(file)) //判断map里是否已经包含某“键-值”  
	{
		return;
	}
	BaseService* ff = ServiceManager::instance()->createServiceObject(ServiceManager::CONVERT_LIVE_TO_HLS);
	if (ff) {
		QSettings s(file, QSettings::IniFormat);
		QString rtsp = s.value("rtsp").toString();
		QString output_directory = s.value("output_directory").toString();
		qDebug() << rtsp;
		qDebug() << output_directory;
		bool enabled = s.value("enabled").toBool();
		ff->setTargetVideoFile(rtsp, output_directory);
		QDateTime dt = QDateTime::currentDateTime();
		int x = dt.daysTo(QDateTime::fromString("2015-10-20", Qt::ISODate));// , Qt::ISODate);
		if (x < 0) return;
		//qDebug() << "tangke:" << x;
		if (ff->open()) {
			Info i = ff->getProcessInfo();
			
			qDebug() << "hProcess:" << (int)i._process_id;
			qDebug() << "next to delete files:" << i._clean_files;

			//CloseHandle(i.hProcess);
			//CloseHandle(i.hThread);
			Info* tmp = new Info((int)i._process_id, i._clean_files);
			processMap[file] = tmp;//(int)i.hProcess;//(void*)(&i);
		}
	}
}

void FileWatcherEngine::ffmpeg_close(const QString& file) {
	//qDebug() << "tk:"<<file;
	BaseService* ff = ServiceManager::instance()->createServiceObject(ServiceManager::CONVERT_LIVE_TO_HLS);
	if (ff) {
		if (processMap.isEmpty()) return; //判断map是否为空  
		//qDebug() << "tk1:" << file;
		if (processMap.contains(file)) //判断map里是否已经包含某“键-值”  
		{
			//qDebug() << "tk2:" << file;
			Info* pi = processMap[file];
			if (pi) {
				qDebug() << "hProcess:" << pi->_process_id;
				//qDebug() << "hThread:" << (int)pi->hThread;
				//qDebug() << "dwProcessId:" << pi->dwProcessId;
				//qDebug() << "dwThreadId:" << pi->dwThreadId;
				if (TerminateProcess((HANDLE)pi->_process_id, 0) > 0) {
					processMap.remove(file);
					qDebug() << "close " << file << " is success";

//					QString tmp = processDirectory[file];
					QString tmp = pi->_clean_files.replace("/", "\\");
					QString cmd = "del /s /a /f /q " + tmp + " >null 2>&1";
					qDebug() << cmd;
					system(cmd.toAscii().data());
				} else {
					qDebug() << "close " << file << " is fail";
				}
			}
		}
	}
}

void FileWatcherEngine::start() {
	
	if (fileSystemWatcher) {
		delete fileSystemWatcher;
		fileSystemWatcher = NULL;
	}
	if (fileMap) {
		delete fileMap;
		fileMap = NULL;
	}
	fileSystemWatcher = new QFileSystemWatcher(this);
	fileMap = new QList<QString>();//new QMap<QString, QDateTime>();
	
	//connect(fileSystemWatcher, SIGNAL(fileChanged(QString)), this, SLOT(fileChanged(QString)));
	connect(fileSystemWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(dirChanged(QString)));

	QString file = QCoreApplication::applicationDirPath() + "/rtsp_hls.ini";
	//QStringList source;

	if (QFile::exists(file)) {
		QSettings s(file, QSettings::IniFormat);
		//	QL_DEBUG << QFile::exists(file);
		monitor_directory = s.value("monitor_directory").toString();
	} else {
		QSettings s(file, QSettings::IniFormat);
		//	QL_DEBUG << QFile::exists(file);
		//s.setValue("source_directory", "g:/taskcity/red5_hls/source");
		//s.setValue("out_directory", "g:/taskcity/red5_hls/out");
		s.setValue("segment_vcodec", "libx264");
		//source_directory = "g:/taskcity/red5_hls";
	}
	QStringList l;
	l << monitor_directory;
	addPaths(l);
}
void FileWatcherEngine::pause() {
	if (fileSystemWatcher) {
		delete fileSystemWatcher;
		fileSystemWatcher = NULL;
	}
	if (fileMap) {
		delete fileMap;
		fileMap = NULL;
	}
}
void FileWatcherEngine::resume() {
	start();
}
