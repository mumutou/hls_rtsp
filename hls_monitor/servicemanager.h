/*
 * ServiceManager.h
 *
 *  Created on: 2012-10-17
 *      Author: Administrator
 */

#ifndef SERVICEMANAGER_H_
#define SERVICEMANAGER_H_

#include <QObject>
#include <QMap>
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
#include <windows.h>
#endif
#include "BaseService.h"


class QTextStream;
class QFile;

class ServiceManager : public QObject {
    Q_OBJECT
public:
    enum ServiceType {
        CONVERT_TO_HLS            = 1 << 0,
		CONVERT_LIVE_TO_HLS			= 1 << 1,
    };
public:
    ServiceManager();
    ~ServiceManager();
    static ServiceManager* instance() {
        if ( !m_instance ) {
            m_instance = new ServiceManager();
        }
        return m_instance;
    }
private:
    static ServiceManager* m_instance;
public:
    void openService ( int type );
    void closeService ( int type );
    void closeAll();
public:
    BaseService* createServiceObject ( int type );

    int m_current_service;
    QMap<int, BaseService* > m_serviceMap;


public slots:
    void closeService();
};

#endif /* SERVICEMANAGER_H_ */
