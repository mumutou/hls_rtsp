/*!
    \module Service

    \title Service Module

    \brief This module is run for the start and close of the third-party programs.
 */

/*!
    \class ServiceManager

    \inmodule Service

    \brief According to the incoming parameters to start or close the third-party programs such as node.exe, fcast.exe.

    \sa ServiceManager::ServiceType, ServiceManager::openService, ServiceManager::closeService
*/

#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QProcess>
//#include <QCoreApplication>
//#include <qlprocess.h>
#include <QMap>
#include <QPointer>
#include <QDateTime>
#include "serviceManager.h"
#include "hlslive.h"

ServiceManager* ServiceManager::m_instance = NULL;

/*! \enum ServiceManager::ServiceType

    This enum describes the different Service Type.

    \value ServiceFile Open or close the \l FileService.

    \value ServiceDiscuss Open or close the \l DiscussService.

    \value ServiceTraining Open or close the \l QuizService.

    \value ServiceExam Open or close the \l ExamService.

    \value ServiceDoc Open or close the DocService.

    \value ServiceDb Open or close the \l MongodbService.

    \value ServiceFileTcp Open or close the \l FileTcpService.

    \value ServiceFileMulticast Open or close the \l FileMulticastService.
*/

/*!
    Constructs a ServiceManager object. ServiceManager will  \l ServiceManager::closeService() first.

    \sa closeService()

 */

ServiceManager::ServiceManager()
    : m_current_service ( 0 ) {
    m_instance = this;
    closeService();
    //  connect(qApp, SIGNAL(aboutToQuit), this, SLOT(stopService()));
}

/*!
    Destructs the object.
*/

ServiceManager::~ServiceManager() {
    // TODO Auto-generated destructor stub
}
/*!
    \internal

    \fn Service* ServiceManager::createServiceObject(int type)

    Create a Service Object according to type \l ServiceManager::ServiceType.

    Returns the Service Object.
 */

BaseService* ServiceManager::createServiceObject ( int type ) {
    BaseService* service = NULL;
	if (type == CONVERT_LIVE_TO_HLS) {
		service = new HLSLive();
	}
    m_serviceMap.insert ( type, service );
    return service;
}

/*!
    \fn void ServiceManager::openService(int type)

    \brief Open a Service according to \l ServiceManager::ServiceType.

    \sa BaseService::open(), ServiceManager::closeService()

 */

void ServiceManager::openService ( int type ) {

    BaseService* service = createServiceObject ( type );
    if ( service->open() ) {
        m_current_service = m_current_service | type;
    } else {
//    	QL_CRITICAL << "open failed";
    }
}

/*!
    \fn void ServiceManager::closeService()

    \brief Close all open Service.    \sa openService()
 */

void ServiceManager::closeService() {
}

void ServiceManager::closeAll() {
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
//	BaseService::close("node.exe");
#else
	BaseService::close("node");
#endif
}
/*!
    If the Service is open, It will close.

    If the Service is not open, it will do nothing.    \sa openService()
 */

void ServiceManager::closeService ( int type ) {
	/*

    if ( m_current_service & type ) {
        BaseService *service = m_serviceMap.take ( type );
		if(!service) return;
        if ( service->close() ) {
            m_current_service = m_current_service ^ type;
        } else {
//            QL_CRITICAL << "close error";

        }
        delete service;
    }
	*/
}
