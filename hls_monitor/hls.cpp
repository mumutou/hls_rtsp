#include "hls.h"
#include "baseservice.h"
#include "filewatcherengine.h"

RTSPHlsService::RTSPHlsService(int argc, char **argv)
: QtService<QCoreApplication>(argc, argv, "RTSP Hls Daemon") {
	setServiceDescription("RTSP Hls Daemon");
	setServiceFlags(QtServiceBase::CanBeSuspended);
	daemon = new FileWatcherEngine;
}


RTSPHlsService::~RTSPHlsService() {
}

void RTSPHlsService::start() {
	BaseService::killProcess("ffmpeg.exe");
	QCoreApplication *app = application();
	if (daemon) {
		daemon->start();
	}
	//app->exec();
}

void RTSPHlsService::pause() {
	BaseService::killProcess("ffmpeg.exe");
	daemon->pause();
}

void RTSPHlsService::resume() {
	daemon->resume();
}

void RTSPHlsService::stop() {
	BaseService::killProcess("ffmpeg.exe");
//	daemon->stop();
}
