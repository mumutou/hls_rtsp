/*!
    \class HLSLive

    \inmodule Service

    \brief
*/

#include "hlslive.h"
#include "baseService.h"
#include <qsettings.h>
#include <qfile.h>
#include <qdir.h>
#include <qfileinfo.h>
#include <qcoreapplication.h>
#include <qdebug.h>
/*!
    \internal

    Constructs a object.
 */
HLSLive::HLSLive()
    : BaseService() {
	this->segment_time = 1;
	this->segment_list_size = 5;
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
	m_application_name = "ffmpeg.exe";
#else
	m_application_name = "node";
#endif
}
/*!
    \internal

    Destructs the object.
*/
HLSLive::~HLSLive() {

}

void HLSLive::getConfig() {
	QString file = QCoreApplication::applicationDirPath() + "/rtsp_hls.ini";
	QSettings s(file, QSettings::IniFormat);
//	QL_DEBUG << QFile::exists(file);
	segment_time = s.value("live_segment_time", 1 ).toInt();
	segment_list_size = s.value("live_segment_list_size", 5).toInt();
	segment_bv = s.value("live_segment_bv").toString();
	segment_vcodec = s.value("live_segment_vcodec").toString();
	segment_acodec = s.value("live_segment_acodec").toString();
	//fps = s.value("fps", 0).toInt();
	//size = s.value("size", 0).toInt();
	aspect = s.value("aspect", "4:3").toString();

	//out_directory = s.value("live_out_directory").toString();
	//source_directory = s.value("live_source_rtmp").toString();
//	QL_DEBUG << tmp_background;
//	int segment_time;
//	int segment_list_size;
//	QString segment_bv;
//	QString segment_vcodec; //视频编码器
//	QString segment_acodec;//音频编码器
//	int fps;//
//	int size;// -s size 设置帧大小 格式为W*H 缺省160X128.也可以直接使用简写
//	int aspect;//     -aspect aspect 设置横纵比 4:3 16:9 或 1.3333 1.7777

}
/*!
    \overload
    \sa BaseService::getServiceOpenCommand()
 */
QString HLSLive::getServiceOpenCommand() {
	return BaseService::getServiceOpenCommand();
}

QStringList HLSLive::getServiceOpenArgs() {
	getConfig();

	QStringList args;

	QString path = this->m_target_video_in.section('/', -1);
//	args << file;
	args << "-i" << this->m_target_video_in;//QString("\"rtmp://%1/%2 live=1\"").arg(source_directory).arg(rtmp_room);
	//if (!segment_vcodec.isEmpty()) {
	//args << "-vcodec" << "libx264";//segment_vcodec;
	//}
	//if (!segment_acodec.isEmpty()) {
	//	args << "-acodec" << segment_acodec;
	//}
	//args << "-flags";
	//args << "-global_header";
	//args << "-strict" << "experimental" << "-acodec" << "aac";
	//args << "-aspect" << aspect;
	//args << "-map" << "0";
	args << "-f" << "hls";
	//if (segment_time != 0) {
	//	args << "-segment_time" << QString("%1").arg(segment_time);
	//}
/*	if (segment_time != 0) {
		args << "-segment_time" << QString("%1").arg(segment_time);
	}*/
	//args << "-segment_format" << "mpegts";
	if (segment_list_size != 0) {
		args << "-hls_list_size" << QString("%1").arg(segment_list_size);
	}
	args << "-hls_wrap" << QString("%1").arg(segment_list_size);//"delete_segments";

//	args << QString("\"%1;
	QDir dir(m_target_video_out);
	if (!dir.exists(path)) {
		dir.mkdir(path);
	}
//	QString path = fi.absoluteDir().path();
	//QString filename = fi.bundleName();

	//qDebug() << "path:" << path;
	//qDebug() << "filename:" << fi.baseName();
	args << "-an";	
	args << QString("\"%1/%2.m3u8\"").arg(m_target_video_out + '/' + path).arg("run");

	//args << "-segment_list_entry_prefix";
	//args << QString("%1/").arg(".");
	//qDebug() << m_target_video_out + '/' + path;
	//QString tmp = QString("\"%1/%05d.ts\"").arg(m_target_video_out + '/' + path);// .arg(rtmp_room);
	//args << tmp;
	this->m_target_files = QString("%1/*").arg(m_target_video_out + '/' + path);
	return args;
}

//HLSLive -re -i test.flv -an -vcodec libx264 -b:v 128k -flags -global_header -map 0 -f segment -segment_time 1 
//-segment_list stream.m3u8 -segment_format mpegts -segment_list_size 800 stream%05d.ts
bool HLSLive::open() {
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
	if (startProcess(getServiceOpenCommand(), getServiceOpenArgs(), QDir::currentPath())) {
#else
	if (startProcess(getServiceOpenCommand(), getServiceOpenArgs(), MyRuntime::instance()->getAppDataPath())) {
#endif
		return true;
	}

	return false;

}