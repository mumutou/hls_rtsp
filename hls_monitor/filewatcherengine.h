/*
 *  Copyright (C) 2010  Giorgio Wicklein <g.wicklein@giowisys.com>
 *
 *  This file is part of DaemonFS.
 *
 *  DaemonFS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DaemonFS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DaemonFS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILEWATCHERENGINE_H
#define FILEWATCHERENGINE_H


//-----------------------------------------------------------------------------
// Hearders
//-----------------------------------------------------------------------------

#include <QObject>
#include <QMap>
#include <qpair.h>
#include <qstringlist.h>

//#include <WinBase.h>
//-----------------------------------------------------------------------------
// Forward declarations
//-----------------------------------------------------------------------------

class QFileSystemWatcher;
class QDateTime;

//-----------------------------------------------------------------------------
// FileWatcherEngine
//-----------------------------------------------------------------------------

class Info : public QObject {
	Q_OBJECT
public:
	Info() {

	}
	Info(int id, QString files) {
		_process_id = id;
		_clean_files = files;
	}
	Info(const Info& i) {
		_process_id = i._process_id;
		_clean_files = i._clean_files;
	}
	Info& operator=(Info& obj) {
		Info* i = new Info(obj._process_id, _clean_files);
		return *i;
	}
public:
	int _process_id;
	QString _clean_files;
public:
	int getProcessId() {
		return _process_id;
	}
	QString getCleanFiles() {
		return _clean_files;
	}
};


class FileWatcherEngine : public QObject
{
    Q_OBJECT

public:
    FileWatcherEngine(QObject *parent = 0);
    ~FileWatcherEngine();

    void addPaths(QStringList&);
    void clear();
//    QDateTime getLastModification(QString);
    bool isDeleted(const QString& path, QString&);
    bool isNew(const QString& path, QString&);

signals:
    void fschanged(QString);
    void addToTableSignal(QString);

private:
    void getAllFiles(QString, QStringList&);

    QFileSystemWatcher *fileSystemWatcher;
    QList<QString> *fileMap;
	QMap<QString, Info*> processMap;
//	QMap<QString, QVariant> processDirectory;

	//QStringList out;
	QString monitor_directory;

private slots:
    void fileChanged(QString);
    void dirChanged(QString);
public:
	void ffmpeg_start(const QString&);
	void ffmpeg_close(const QString&);
public:
	void start();
	void pause();
	void resume();
};

#endif // FILEWATCHERENGINE_H
