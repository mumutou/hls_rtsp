#ifndef HLS_H
#define HLS_H

#include "libs/qtservice.h"
class FileWatcherEngine;
class RTSPHlsService : public QtService<QCoreApplication> {
public:
	RTSPHlsService(int argc, char **argv);
/*		: QtService<QCoreApplication>(argc, argv, "RTSP Hls Daemon") {
		setServiceDescription("RTSP Hls Daemon");
		setServiceFlags(QtServiceBase::CanBeSuspended);
		daemon = new FileWatcherEngine;
	}*/
	~RTSPHlsService();
protected:
	void stop();
	void start();/* {
		QCoreApplication *app = application();
		if (daemon) {
			daemon->start();
		}
	}*/

	void pause();/* {
		daemon->pause();
	}*/

	void resume(); /*{
		daemon->resume();
	}*/

private:
	FileWatcherEngine *daemon;
};

#endif