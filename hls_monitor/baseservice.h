#ifndef SERVICE_H
#define SERVICE_H

#include <QObject>
#include <QFile>
#include <qpair.h>
#include "filewatcherengine.h"
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
#include <windows.h>
#endif


class BaseService : public QObject {
    Q_OBJECT
public:
    BaseService();
    ~BaseService();

public:
    virtual bool open();
    virtual bool close();

    static bool close(const QString& process_name);
protected:
    bool startProcess ( const QString& process, const QStringList& arg, const QString& env );

    virtual QString getServiceOpenCommand();
	virtual QStringList getServiceOpenArgs();

protected:
	QString m_application_name;
	QString m_target_video_in;
	QString m_target_video_out;
	QString m_target_files;
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
    PROCESS_INFORMATION m_pinfo;
public:
	Info getProcessInfo() {
		return Info((int)m_pinfo.hProcess, m_target_files);
	}
#endif
public:
	virtual void setTargetVideoFile(const QString& in, const QString& out) {
		m_target_video_in = in;
		m_target_video_out = out;
	}
	//static bool getProcessIsRunning(const QString& string);
	//static bool getProcessIsRunning(int processid);
	static bool killProcess(const QString& string);
	//static bool killProcess(int processid);
	//static int getProcessId(const QString& string);
	//static QString getProcessString(int processid);
};

#endif
