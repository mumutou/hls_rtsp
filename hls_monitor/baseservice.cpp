/*!
    \class BaseService

    \inmodule Service

    \brief
*/
#include "BaseService.h"


//#include <BaseService.h>
#include <qfile.h>
#include <qprocess.h>
#include <qdir.h>
#include <qdebug.h>
#include <qcoreapplication.h>
#include <qsettings.h>
#include <windows.h>
#include <tlhelp32.h>
void echo(const QString& key, const QString& value) {
	QString file = QCoreApplication::applicationDirPath() + "/echo.ini";
	QSettings s(file, QSettings::IniFormat);
	//	QL_DEBUG << QFile::exists(file);
	s.setValue(key, value);
}

/*!
    Constructs a Service object.
 */

BaseService::BaseService() {
	
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
    m_application_name = "ffmpeg.exe";
#else
    m_application_name = "node";
#endif
}

/*!
    Destructs the BaseService object.
*/
BaseService::~BaseService() {
}
/*!
    Returns Config Template FileName

    \warning Config Template FileName is fullpath Name.

    \warning If the System is Windows, must add double quotes if string contains blank space.

  */

/*!
    Open the Config FileName

    Return the \l QFile If Open successfully.Otherwise return NULL.

    \warning Config FileName is fullpath Name.

    \warning If the System is Windows, must add double quotes if string contains blank space.

    \sa QFile::open()

  */


/*!
    Returns BaseService Open Command Line

    \warning If the System is Windows, must add double quotes if string contains blank space.

  */
QString BaseService::getServiceOpenCommand() {
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
	QString process = QString("\"") + QCoreApplication::applicationDirPath() + QString("/" + m_application_name + "\" ");
#else
    QString process = MyRuntime::instance()->getAppDataPath() + QString ( "/nodejs/" + m_application_name );
#endif
    return process;

//    return QString ( "" );
}

QStringList BaseService::getServiceOpenArgs() {
    return QStringList ( "" );
}

/*!
  \internal
*/
bool BaseService::startProcess ( const QString& process, const QStringList& arg, const QString& env ) {
//    QL_DEBUG << "process:" << process << "arg:" << arg << "env:" << env;

    bool result;
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
    PROCESS_INFORMATION pinfo;
    pinfo.hProcess = 0;
    pinfo.hThread = 0;
    pinfo.dwProcessId = 0;
    pinfo.dwThreadId = 0;
    STARTUPINFOW startupInfo = { sizeof ( STARTUPINFO ), 0, 0, 0,
                                 ( ulong ) CW_USEDEFAULT, ( ulong ) CW_USEDEFAULT,
                                 ( ulong ) CW_USEDEFAULT, ( ulong ) CW_USEDEFAULT, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0
                               };
    startupInfo.wShowWindow = SW_HIDE;
    startupInfo.dwFlags = STARTF_USESHOWWINDOW;

	QString runs = process;
	for(int i = 0 ; i < arg.size(); i ++) {
		runs += " ";
		runs += arg[i];
	}
//	qDebug() << runs;
	qDebug() << "start to convert.....";

//	system("dir > d:/tmp");
//	echo("commands", runs);

//	return true;
    //QL_DEBUG << "process:" << runs;
    int ret = CreateProcess ( 0, ( wchar_t* ) runs.utf16(), 0, 0, FALSE,
                              CREATE_UNICODE_ENVIRONMENT | CREATE_NEW_CONSOLE, 0,
                              ( wchar_t* ) env.utf16(),
                              &startupInfo, &pinfo );
    if ( ret == 0 ) {
        //失败
        result = false;
       // QL_CRITICAL << GetLastError();

    } else {
        result = true;

        m_pinfo = pinfo;
    }

#else
	return QProcess::startDetached(process, arg, env);
#endif
    return result;
}
/*!
    \fn bool BaseService::close()

    \brief Close this Service.

    Returns true always.
*/
bool BaseService::close() {
    //return BaseService::killProcess ( m_application_name );
	return true;
}

bool BaseService::close(const QString& process) {
    //return BaseService::killProcess ( process );
	return true;
}

/*!
\fn bool BaseService::open()

\brief Open the Service

\sa close()
*/
bool BaseService::open() {
#if defined(Q_WS_WIN) || defined(Q_OS_WIN32)
	if (startProcess(getServiceOpenCommand(), getServiceOpenArgs(), QDir::currentPath())) {
#else
	if (startProcess(getServiceOpenCommand(), getServiceOpenArgs(), MyRuntime::instance()->getAppDataPath())) {
#endif
		return true;
	}

	return false;

}


/*!
Return true when the process named \a string is running.
*/
/*
bool BaseService::getProcessIsRunning(const QString& string) {
	PROCESSENTRY32 pe32;

	pe32.dwSize = sizeof(pe32);

	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);
	while (bMore) {
		if (!string.compare(QString::fromWCharArray(static_cast<const wchar_t*>(pe32.szExeFile)))) {
			return true;
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}

	return false;
}*/
/*!
Return true when the process which id is \a processid is running.
*/
/*
bool BaseService::getProcessIsRunning(int processid) {
	PROCESSENTRY32 pe32;

	pe32.dwSize = sizeof(pe32);

	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);
	while (bMore) {
		if ((DWORD)processid == pe32.th32ProcessID) {
			return true;
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}

	return false;
}*/
/*!
Return true when process named \a string is killed successfully.
*/
bool BaseService::killProcess(const QString& string) {
	//echo("commands2", "killprocess");
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(pe32);
	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);
//	QL_DEBUG;
	while (bMore) {
		if (!string.compare(QString::fromUtf16((const ushort*)(pe32.szExeFile)))) {
			HANDLE hProcessHandle = ::OpenProcess(PROCESS_TERMINATE, FALSE,
				pe32.th32ProcessID);
			::TerminateProcess(hProcessHandle, 4);
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}
	return false;
	/*
	int CStaticFunc::KillProcess(LPCSTR pszClassName, LPCSTR
	pszWindowTitle)
	{
	HANDLE hProcessHandle;
	ULONG nProcessID;
	HWND TheWindow;
	TheWindow = ::FindWindow( NULL, pszWindowTitle );
	::GetWindowThreadProcessId( TheWindow, &nProcessID );
	hProcessHandle = ::OpenProcess( PROCESS_TERMINATE, FALSE,
	nProcessID );
	return ::TerminateProcess( hProcessHandle, 4 );
	}
	return false;
	*/
}
/*!
Return true when process which id is \a processid is killed successfully.
*/
/*
bool BaseService::killProcess(int processid) {
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(pe32);
	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);

	while (bMore) {
		if ((DWORD)processid == pe32.th32ProcessID) {
			HANDLE hProcessHandle = ::OpenProcess(PROCESS_TERMINATE, FALSE,
				pe32.th32ProcessID);
			return ::TerminateProcess(hProcessHandle, 4);
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}
	return false;
}*/
/*!
Return the id of process which name is \a string.
*/
/*
int BaseService::getProcessId(const QString& string) {
	PROCESSENTRY32 pe32;

	pe32.dwSize = sizeof(pe32);

	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);
	while (bMore) {
		if (!string.compare(QString::fromWCharArray(static_cast<const wchar_t*>(pe32.szExeFile)))) {
			return (int)pe32.th32ProcessID;
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}

	return -1;
}*/
/*!
Return the process which id is \a processid.
*/
/*
QString BaseService::getProcessString(int processid) {
	PROCESSENTRY32 pe32;

	pe32.dwSize = sizeof(pe32);

	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	BOOL bMore = ::Process32First(hProcessSnap, &pe32);
	while (bMore) {
		if ((DWORD)processid == pe32.th32ProcessID) {
			return QString::fromWCharArray(static_cast<const wchar_t*>(pe32.szExeFile));
		}
		bMore = ::Process32Next(hProcessSnap, &pe32);
	}

	return QString("");
}*/

