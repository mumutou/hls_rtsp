#ifndef HLSLIVE_H
#define HLSLIVE_H

#include "baseService.h"

//HLS直播处理类
class HLSLive : public BaseService {
    Q_OBJECT

public:
    HLSLive();
    ~HLSLive();
public:
	bool open();
protected:
	QString out_directory;
	QString source_directory;
	//QString rtmp_room;
	int segment_time; //每个分片的持续时间
	int segment_list_size; //最大分片数量
	QString segment_bv; //码流
	QString segment_vcodec; //视频编码器
	QString segment_acodec;//音频编码器
	
	QString aspect;

	//HLSLive -v 9 -loglevel 99 -re -i test.flv -an -vcodec libx264 -b:v 128k -flags -global_header -map 0 -f segment -segment_time 1 -segment_list stream.m3u8 -segment_format mpegts -segment_list_size 800 stream%05d.ts
protected:
    virtual void getConfig();
    virtual QString getServiceOpenCommand();
	virtual QStringList getServiceOpenArgs();
public:
	/*void setRoom(const QString& room) {
		rtmp_room = room;
	}*/
};

#endif
/*
*/