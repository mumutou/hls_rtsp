
#include <QtCore/QCoreApplication>
#include <qstringlist.h>
#include "hls.h"
#include "filewatcherengine.h"
#include "hlslive.h"
#include "servicemanager.h"
//#include "main.moc"

int main(int argc, char *argv[])
{
	QStringList args;
	for (int i = 0; i < argc; ++i) {
		args.append(QString::fromLocal8Bit(argv[i]));
	}
	if (args.size() > 1) {
		QString a = args.at(1);
		if (a == QLatin1String("-debug") || a == QLatin1String("-local")) {
			QCoreApplication app(argc, argv);
			FileWatcherEngine* daemon = new FileWatcherEngine;
			if (daemon) {
				BaseService::killProcess("ffmpeg.exe");
				daemon->start();
			}
			return app.exec();
		} else if (a == QLatin1String("-h") || a == QLatin1String("-help")) {
			printf("version: 1.0.1 \n");
			printf("rtsp_hls \n");
			printf("\t open service \n");
			printf("rtsp_hls -debug \n");
			printf("\t run as debug mode  \n");
			printf("rtsp_hls -i \n");
			printf("\t install service \n");
			printf("rtsp_hls -uninstall \n");
			printf("\t uninstall service \n");
			//printf("rtsp_hls -live(live mode, experiment) \n");
			//printf("\t open a process to convert rtmp as hls \n");
			printf("rtsp_hls -h|-help \n");
			printf("\t show help \n");
		} else if (a == QLatin1String("-live")) {
			if (args.size() > 2) {
				//QString room = args.at(2);
				QCoreApplication app(argc, argv);
				HLSLive* p = (HLSLive*)ServiceManager::instance()->createServiceObject(ServiceManager::CONVERT_LIVE_TO_HLS);
				
				if (p) {
					//p->setRoom(room);
					p->open();
					return app.exec();
				} else {
					return 1;
				}
			}
		} else {
			RTSPHlsService service(argc, argv);

			return service.exec();
		}
	} else {
		RTSPHlsService service(argc, argv);

		return service.exec();
	}
}
